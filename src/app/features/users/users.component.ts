import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  template: `
  `,
})
export class UsersComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    // FILTER, MAP, REDUCE or array
    this.http.get<any[]>('http://localhost:3000/users')
      .subscribe(
        val => console.log(val),
        err => console.log('error UI', err)
      )
  }

}
