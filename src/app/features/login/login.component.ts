import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  template: `
    <p>
      Login Works!!
      
      {{authService.token$ | async}}
      
      <button (click)="signIn()">SIGN IN</button>
    </p>
  `,
  styles: [
  ]
})
export class LoginComponent{
  constructor(public authService: AuthService) {
   this.authService.isLogged$()
      .pipe(
        filter(isLogged => !!isLogged)
      )
      .subscribe(() => {
        console.log('redirect');
      });
  }

  signIn() {
    this.authService.login()
  }
}
