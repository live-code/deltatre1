import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AuthService } from './core/auth/auth.service';
import { PanelComponent } from './shared/components/panel.component';
import { ChartComponent } from './shared/components/chart.component';

@Component({
  selector: 'app-root',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: ` 
    
    <button routerLink="/custom">Customers</button>
    <button routerLink="/users">users</button>
    <!--<button routerLink="/login">login</button>-->
    <button ifLogged role="guest" routerLink="/admin">admin</button>
    
    <button (click)="auth.logout()" *ifSignIn>logout</button>
    
    <hr>
    <router-outlet></router-outlet>

    <button url="http://www.deltatre.com">By D3</button>

    <div 
      *ngFor="let widget of widgets" 
      [loader]="widget"

    ></div>
  `,
  styles: [],
})
export class AppComponent {
  widgets = [
    { type: PanelComponent, payload: { title: 'panelo'}},
    { type: ChartComponent, payload: {config: [1, 2, 3, 4]}},
  ];
  constructor(public auth: AuthService) {
  }
}
