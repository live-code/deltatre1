import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileComponent } from './features/profile/profile.component';
import { ProfileModule } from './features/profile/profile.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './core/auth/auth.interceptor';
import { AuthService } from './core/auth/auth.service';
import { IfloggedDirective } from './core/auth/iflogged.directive';
import { UrlDirective } from './shared/url.directive';
import { IfSigninDirective } from './core/auth/if-signin.directive';
import { LoaderDirective } from './shared/loader';
import { PanelComponent } from './shared/components/panel.component';
import { ChartComponent } from './shared/components/chart.component';

@NgModule({
  declarations: [
    AppComponent,
    IfloggedDirective,
    UrlDirective,
    IfSigninDirective,
    LoaderDirective,
    PanelComponent,
    ChartComponent
  ],
  imports: [
    BrowserModule,
    ProfileModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
