import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[url]'
})
export class UrlDirective {
  @Input() url: string;

  @HostListener('click')
  clickHandler() {
    window.open(this.url);
  }
}
