import {
  Directive, ElementRef, ViewContainerRef, Input,
  ComponentFactoryResolver, ComponentRef, OnDestroy,
} from '@angular/core';
@Directive({
  selector: '[loader]'
})
export class LoaderDirective implements OnDestroy {
  constructor(
    private view: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) { }

  private ref: ComponentRef<any>;

  @Input() set loader(component) {
    this.view.clear();
    if (component) {
      console.log(component)
      const factory = this.resolver.resolveComponentFactory(component.type);
      this.ref = this.view.createComponent(factory);

      for (const key in component.payload) {
        this.ref.instance[key] = component.payload[key];
        // this.ref.instance.open.subscribe(e => this.close.emit())
      }
    }
  }
  ngOnDestroy() {
    if (this.ref) {
      this.ref.destroy();
    }
  }
}
