import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-panel',
  template: `
    <p>
      panel works! {{title}}
    </p>
  `,
  styles: [
  ]
})
export class PanelComponent implements OnInit {
  @Input() title: string
  constructor() { }

  ngOnInit(): void {
  }

}
