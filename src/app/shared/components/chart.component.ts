import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-chart',
  template: `
    <p>
      chart works! {{config | json}}
    </p>
  `,
  styles: [
  ]
})
export class ChartComponent implements OnInit {
  @Input() config: string
  constructor() { }

  ngOnInit(): void {
  }

}
