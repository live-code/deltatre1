import { Directive, ElementRef, HostBinding, Input, OnInit, Renderer2, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from './auth.service';
import { tap } from 'rxjs/operators';

@Directive({
  selector: '[ifSignIn]',
})
export class IfSigninDirective implements OnInit {
  constructor(
    private auth: AuthService,
    private template: TemplateRef<any>,
    private view: ViewContainerRef
  ) {}

  ngOnInit() {
    this.auth.isLogged$()
      .subscribe(isLogged => {
        this.view.clear()
        if (isLogged) {
          this.view.createEmbeddedView(this.template)
        }
      });
  }
}
