import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map, share, tap } from 'rxjs/operators';

interface Auth {
  token: string;
  role: string;
  profile: any;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token$: BehaviorSubject<string> = new BehaviorSubject(null);

  constructor(private http: HttpClient) {
    this.token$.next(
      localStorage.getItem('token')
    );
  }

  login() {
    this.http.get<{ token: string}>('http://localhost:3000/login')
      .subscribe(res => {
        this.token$.next(res.token);
        localStorage.setItem('token', res.token);
      });
  }

  logout() {
    this.token$.next(null);
    localStorage.removeItem('token')

  }

  isLogged$(): Observable<boolean> {
    return this.token$.pipe(
      map(tk => !!tk),
    );
  }

  get token(): string {
    return this.token$.getValue();
  }
}
