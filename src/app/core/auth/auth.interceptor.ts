import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { root } from 'rxjs/internal-compatibility';
import { iif, Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, filter, first, mergeMap, mergeMapTo, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({ providedIn: root })
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.isLogged$()
      .pipe(
        mergeMapTo(this.authService.token$),
        first(),
        mergeMap(token => {
          return iif(
            () => !!token,
            next.handle(req.clone({ setHeaders: { Authorization: token} })),
            next.handle(req)
          );
        }),
        catchError(err => {
          this.handleErrors(err.status)
          // return of(err);
          return throwError(err);
        })
      );
  }


  handleErrors(status: number) {
    switch (status) {
      case 401:
      case 404:
        this.authService.logout();
        this.router.navigateByUrl('login');
        break;
    }
  }
}


// SOLUTION 2
/*
@Injectable({ providedIn: root })
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.authService.isLogged$()
      .pipe(
        mergeMapTo(this.authService.token$),
        mergeMap(token => {
          let clonedReq = req;
          if (!!token) {
            clonedReq = req.clone({
              setHeaders: { Authorization: token}
            });
          }
          return next.handle(clonedReq);
        })
      );
  }
}
*/

// SOLUTION 1
/*
@Injectable({ providedIn: root })
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let clonedReq = req;
    if (this.authService.isLogged()) {
      clonedReq = req.clone({
        setHeaders: { Authorization: this.authService.token}
      });
    }

    return next.handle(clonedReq);
  }
}
*/
