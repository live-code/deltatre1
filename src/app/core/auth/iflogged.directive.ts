import { Directive, ElementRef, HostBinding, Input, OnInit, Renderer2 } from '@angular/core';
import { AuthService } from './auth.service';

@Directive({
  selector: '[ifLogged]',
})
export class IfloggedDirective implements OnInit {
  isLogged: boolean;
  @Input() role: string;
/*
  @HostBinding('style.display') get innerHTML() {
    console.log('ciao ciao')
    return this.isLogged ? null : 'none';
  }
*/

  constructor(
    private auth: AuthService,
    private el: ElementRef,
    private renderer: Renderer2
  ) {}

  ngOnInit() {
    this.auth.isLogged$()
      .subscribe(isLogged => {
        console.log(this.role)
        if (isLogged) {
          this.renderer.setStyle(this.el.nativeElement, 'display', null);
        } else {
          this.renderer.setStyle(this.el.nativeElement, 'display', 'none');
        }
      });
  }
}
